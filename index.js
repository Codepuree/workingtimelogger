var fs = require('fs');
var program = require('commander');
var chalk = require('chalk');

const dataPath = './working_times.json';
var data = null;

let currentDate = new Date();
let yyyymmdd = currentDate.toISOString().slice(0, 10);

if (!fs.existsSync(dataPath)) {
    data = {
        meta: {
            start: new Date()
        },
        dates: {}
    };
} else {
    data = require(dataPath);
}

// options
program
    .option('-b --begin <task>', 'Adds time with time-type \'begin\' and type enum (work w, break b)')
    .option('-e --end', 'Adds time with time-type \'end\' and type enum (work w, break b)')
    .option('-c --comment <comment>', 'Adds an comment')
    .option('-l --log [range]', 'Logs the data to the console, range can be (day, month, year, all)')
    .option('-o --output <file>', 'Stores the data in the given file')
    .parse(process.argv)


// Test
if (program.begin) {
    var time = {
        time: currentDate,
        type: 'begin'
    };

    if (program.begin[0] == 'w') {
        time.task = 'work';
    } else if (program.begin[0] == 'b') {
        time.task = 'break';
    } else {
        // TODO: Throw error, missing or wrong argument type
    }

    if (program.comment) {
        time.comment = program.comment;
    }

    if (data.dates[yyyymmdd] && data.dates[yyyymmdd].length > 0) {
        let last = data.dates[yyyymmdd][data.dates[yyyymmdd].length - 1];

        if (last.task != time.task) {
            addToDate({
                time: currentDate,
                type: last.type == 'begin' ? 'end' : 'begin',
                task: last.task,
            });
            addToDate(time);
        } else if (last.type == 'end') {
            addToDate(time);
        }
    } else {
        addToDate(time);
    }
} else if (program.end) {
    // var time = {
    //     time: currentDate,
    //     type: 'end'
    // };

    // if (program.end[0] == 'w') {
    //     time.task = 'work';
    // } else if (program.end[0] == 'b') {
    //     time.task = 'break';
    // } else {
    //     // TODO: Throw error, missing or wrong argument type
    // }

    // if (program.comment) {
    //     time.comment = program.comment;
    // }
    // addToDate(time);
    if (data.dates[yyyymmdd] && data.dates[yyyymmdd].length > 0) {
        let last = data.dates[yyyymmdd][data.dates[yyyymmdd].length - 1];

        if (last.type == 'begin') {
            let time = {
                time: currentDate,
                type: 'end',
                task: last.task
            };

            if (program.comment) {
                time.comment = program.comment;
            }

            addToDate(time);
        }
    }

} else if (program.log) {
    if (typeof (program.log) == 'boolean') {
        log_current();
    } else {
        console.log(program.log);
    }
}

function addToDate(obj) {
    if (!data.dates[yyyymmdd]) {
        data.dates[yyyymmdd] = [];
    }

    data.dates[yyyymmdd].push(obj);
    fs.writeFileSync(dataPath, JSON.stringify(data));
}

function log_current() {
    if (!data.dates[yyyymmdd]) {
        console.log('Nothing done today!')
    } else {
        const minWorkingTime = 8 * 60 * 60 * 1000;    // 8h
        const maxBreakTime = 1 * 60 * 60 * 1000;    // 1h
        let workingTime = 0;
        let breakTime = 0;

        // Temp
        let last_start_work = null;
        let last_start_break = null;
        for (let i = 0; i < data.dates[yyyymmdd].length; i++) {
            let time = data.dates[yyyymmdd][i];

            if (time.type == 'begin') {
                if (time.task == 'work') {
                    last_start_work = new Date(time.time);
                } else if (time.task == 'break') {
                    last_start_break = new Date(time.time);
                }
            } else if (time.type == 'end') {
                if (time.task == 'work') {
                    let last_stop_work = new Date(time.time);

                    if (last_start_work) {
                        workingTime += last_stop_work - last_start_work;
                    }
                } else if (time.task == 'break') {
                    let last_stop_break = new Date(time.time);

                    if (last_start_break) {
                        breakTime += last_stop_break - last_start_break;
                    }
                }
            }
        }

        totalTime = workingTime + breakTime;

        // Log to console
        console.log('You are here since ' + chalk.yellow(msToStr(totalTime)) + '!');
        console.log('Work: ' + chalk.yellow(msToStr(workingTime)) + '\t\t\tBreak: ' + chalk.yellow(msToStr(breakTime)));

        // Progress
        let prog = '     ';
        let maxWidth = process.stdout.columns - 15;
        let per = Math.ceil(maxWidth * workingTime / minWorkingTime);
        for (let i = 0; i < maxWidth; i++) {
            if (i < per) {
                prog += chalk.bgYellow(' ');
            } else {
                prog += chalk.bgWhite(' ');
            }
        }
        console.log(prog);

        // Overtime
        let overtime = workingTime - minWorkingTime;
        if (overtime > 0) {
            console.log('Overtime: ' + chalk.yellow(msToStr(overtime)));
        }

        // Long breaktime
        let overBreakTime = breakTime - maxBreakTime;
        if (overBreakTime > 0) {
            console.log('Over breaktime: ' + chalk.yellow(msToStr(overBreakTime)));
        }
    }

    function msToStr(milliseconds) {
        let h = Math.floor(milliseconds / (60 * 60 * 1000));
        let th = milliseconds - (h * 60 * 60 * 1000);
        let m = Math.floor(th / (60 * 1000));
        let tm = th - (m * 60 * 1000);
        let s = tm / 1000;

        let out = '';
        if (h > 0) out += h + 'h ';
        if (h > 0 || m > 0) out += m + 'm ';
        out += s.toFixed(2) + 's'

        return out;
    }
}